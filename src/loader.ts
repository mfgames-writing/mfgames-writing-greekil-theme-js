import { GreekilTheme } from "./GreekilTheme";

function loadGreekilTheme()
{
    var theme = new GreekilTheme();
    return theme;
}

export default loadGreekilTheme;
